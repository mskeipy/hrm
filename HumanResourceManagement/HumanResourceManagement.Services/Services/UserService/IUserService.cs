﻿using System.Collections.Generic;
using HumanResourceManagement.Models.Models;

namespace HumanResourceManagement.Services.Services.UserService
{
    public interface IUserService
    {
        User Authenticate(string username, string password);
        User GetById(int? id);
        IEnumerable<User> GetAll();
        void Add(User user);
        void Delete(int id);
        void Update(User user);
        void Save();

    }
}