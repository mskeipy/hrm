﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using HumanResourceManagement.Models.Models;
using HumanResourceManagement.Services.Extension;
using HumanResourceManagement.UnitOfWork.UnitOfWork;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace HumanResourceManagement.Services.Services.UserService
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly AppSetting _appSettings;
       
        public UserService(IUnitOfWork unitOfWork, IOptions<AppSetting> appSettings)
        {
            _unitOfWork = unitOfWork;
            _appSettings = appSettings.Value;
        }
        
        private List<User> _users = new List<User>
        { 
            new User { Id = 1, FirstName = "Admin", LastName = "User", Username = "admin", Password = "admin", Role = Role.Admin },
            new User { Id = 2, FirstName = "Normal", LastName = "User", Username = "user", Password = "user", Role = Role.User } 
        };
    
        public User GetById(int? id)
        {
            return _unitOfWork.UserReponsitory.GetById(id);
        }

        public IEnumerable<User> GetAll()
        {
            return _unitOfWork.UserReponsitory.GetAll();
        }

        public void Add(User user)
        {
            _unitOfWork.UserReponsitory.Add(user);
        }

        public void Delete(int id)
        {
            _unitOfWork.UserReponsitory.Delete(id);
        }

        public void Update(User user)
        {
            _unitOfWork.UserReponsitory.Update(user);
        }

        public void Save()
        {
            _unitOfWork.UserReponsitory.Save();
        }
        
        public User Authenticate(string username, string password)
        {
            var user = _users.SingleOrDefault(x => x.Username == username && x.Password == password);

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            // remove password before returning
            user.Password = null;

            return user;
        }

    }
}