﻿using Microsoft.EntityFrameworkCore;

namespace HumanResourceManagement.Models.Models
{
    public class HRMDBContext : DbContext
    {
        public HRMDBContext(DbContextOptions<HRMDBContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

    }
}