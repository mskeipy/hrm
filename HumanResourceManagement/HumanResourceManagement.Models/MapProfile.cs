﻿using AutoMapper;
using HumanResourceManagement.Models.Models.ViewModels;

namespace HumanResourceManagement.Models.Models
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            CreateMap<User, UserViewModel>();
        }

    }
}