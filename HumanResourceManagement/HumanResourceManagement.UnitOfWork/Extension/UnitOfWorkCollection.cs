﻿using HumanResourceManagement.UnitOfWork.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace HumanResourceManagement.UnitOfWork.Extension
{
    public static class UnitOfWorkCollection
    {
        public static IServiceCollection AddUnitOfWork<HRMDBContext>(this IServiceCollection service)
            where HRMDBContext : DbContext
        {
            service.AddTransient<IUnitOfWork, UnitOfWork.UnitOfWork>();
            return service;
        }
    }
}