﻿using System;
using HumanResourceManagement.Models.Models;
using HumanResourceManagement.Reponsitory.Reponsitory;

namespace HumanResourceManagement.UnitOfWork.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private HRMDBContext _context;

        public UnitOfWork(HRMDBContext context)
        {
            _context = context;
            InitReponsitory();
        }

        private bool _disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IGenericReponsitory<User> UserReponsitory { get; private set; }
        
        private void InitReponsitory()
        {
            UserReponsitory = new GenericReponsitory<User>(_context);
        }
        public void Save()
        {
            _context.SaveChanges();
        }

    }
}