﻿using System;
using HumanResourceManagement.Models.Models;
using HumanResourceManagement.Reponsitory.Reponsitory;

namespace HumanResourceManagement.UnitOfWork.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericReponsitory<User> UserReponsitory { get; }
        void Save();
    }
}