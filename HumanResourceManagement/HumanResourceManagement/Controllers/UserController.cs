﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using HumanResourceManagement.Models.Models;
using HumanResourceManagement.Models.Models.ViewModels;
using HumanResourceManagement.Services.Extension;
using HumanResourceManagement.Services.Services.UserService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Serilog;

namespace HumanResourceManagement.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly AppSetting _appSettings;

        public UserController(IUserService userService, IMapper mapper, IOptions<AppSetting> appSettings)
        {
            _userService = userService;
            _mapper = mapper;
            _appSettings = appSettings.Value;

        }
        
//        [HttpPost("authenticate")]
//        public IActionResult Authenticate([FromBody] User users)
//        {
//            var user = _userService.Authenticate(users.Username, users.Password);
//
//            if (user == null)
//                return BadRequest(new {message = "Username or password is incorrect"});
//
//            var tokenHandler = new JwtSecurityTokenHandler();
//            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
//            var tokenDescriptor = new SecurityTokenDescriptor
//            {
//                Subject = new ClaimsIdentity(new Claim[]
//                {
//                    new Claim(ClaimTypes.Name, users.Id.ToString())
//                }),
//                Expires = DateTime.UtcNow.AddDays(7),
//                SigningCredentials =
//                    new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
//            };
//            var token = tokenHandler.CreateToken(tokenDescriptor);
//            var tokenString = tokenHandler.WriteToken(token);
//
//            // return basic user info (without password) and token to store client side
//            return Ok(new
//            {
//                Id = users.Id,
//                Username = users.Username,
//                FirstName = users.FirstName,
//                LastName = users.LastName,
//                Token = tokenString
//            });
//        }

        [HttpGet]
        public async Task<IEnumerable<User>> Get()
        {
            var users = _userService.GetAll();
            return users;
        }

        // GET: api/Todo/5
        [HttpGet("{id}")]
        public async Task<User> GetId(int? id)
        {
            var users = _userService.GetById(id);
            return users;
        }
        
        [HttpPost]
        public void Post(User users)
        {
            _userService.Add(users);
            _userService.Save();
        }
        
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, User users)
        {
            if (id != users.Id)
            {
                return BadRequest();
            }
            _userService.Update(users);
            _userService.Save();
            return NoContent();
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var todoItem = _userService.GetById(id);

            if (todoItem == null)
            {
                return StatusCode(Microsoft.AspNetCore.Http.StatusCodes.Status204NoContent);
            }

            _userService.Delete(id);
            _userService.Save();

            return NoContent();
        }




    }
}