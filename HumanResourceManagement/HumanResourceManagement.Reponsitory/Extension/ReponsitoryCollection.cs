﻿using HumanResourceManagement.Models.Models;
using HumanResourceManagement.Reponsitory.Reponsitory;
using HumanResourceManagement.Reponsitory.Reponsitory.UserReponsitory;
using Microsoft.Extensions.DependencyInjection;

namespace HumanResourceManagement.Reponsitory.Extension
{
    public static class ReponsitoryCollection
    {
        public static IServiceCollection AddRepositoriesInstance(this IServiceCollection services)
        {
            services.AddTransient<IGenericReponsitory<User>, GenericReponsitory<User>>();
            services.AddTransient<IUserReponsitory, UserReponsitory>();

            return services;
        }
    }
}