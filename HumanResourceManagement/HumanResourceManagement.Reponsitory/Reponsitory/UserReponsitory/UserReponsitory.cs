﻿using HumanResourceManagement.Models.Models;

namespace HumanResourceManagement.Reponsitory.Reponsitory.UserReponsitory
{
    public class UserReponsitory : GenericReponsitory<User>, IUserReponsitory
    {
        public UserReponsitory(HRMDBContext context) : base(context)
        {
        }
    }
}