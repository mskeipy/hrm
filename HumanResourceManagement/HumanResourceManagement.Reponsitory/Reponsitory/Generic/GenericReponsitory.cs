﻿using System.Collections.Generic;
using HumanResourceManagement.Models.Models;
using Microsoft.EntityFrameworkCore;

namespace HumanResourceManagement.Reponsitory.Reponsitory
{
    public class GenericReponsitory<TEntity> : IGenericReponsitory<TEntity> where TEntity : class
    {
        private readonly HRMDBContext _context;
        private DbSet<TEntity> _dbSet;

        public GenericReponsitory(HRMDBContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public TEntity GetById(int? id)
        {
            return _dbSet.Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet;
        }

        public void Add(TEntity entity)
        {
            _dbSet.Add(entity);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            TEntity entity = _dbSet.Find(id);
            _dbSet.Remove(entity);
        }

        public void Update(TEntity entity)
        {
            _dbSet.Update(entity);
            _context.SaveChanges();
        }

        public void Save()
        {
            _context.SaveChanges();
        }


    }
}