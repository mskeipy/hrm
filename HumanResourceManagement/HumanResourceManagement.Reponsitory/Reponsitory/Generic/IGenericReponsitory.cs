﻿using System.Collections.Generic;

namespace HumanResourceManagement.Reponsitory.Reponsitory
{
    public interface IGenericReponsitory<TEntity>
    {
        TEntity GetById(int? id);
        IEnumerable<TEntity> GetAll();
        void Add(TEntity entity);
        void Delete(int id);
        void Update(TEntity entity);
        void Save();

    }
}